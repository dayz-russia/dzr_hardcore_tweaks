class dzrSearchKindlingCB : ActionContinuousBaseCB
{
	
	override void CreateActionComponent()
	{

		m_ActionData.m_ActionComponent = new CAContinuousTime(90);
		
	}
};

class dzrSearchKindling: ActionContinuousBase
{
	void dzrSearchKindling()
	{
		m_CallbackClass = dzrSearchKindlingCB;
			
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONFB_DEPLOY_2HD; //Call the animation	
		m_FullBody = true;
		m_StanceMask = DayZPlayerConstants.STANCEMASK_CROUCH | DayZPlayerConstants.STANCEMASK_ERECT;
		m_SpecialtyWeight = UASoftSkillsWeight.ROUGH_LOW;
	}
	
	override void CreateConditionComponents()  
	{
		m_ConditionTarget = new CCTSurface(UAMaxDistances.DEFAULT);
		m_ConditionItem = new CCINotPresent;
	}
	
	override string GetText()
	{
		return "#STR_DZR_SearchBark";
	}
	

	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		if ( !target )
			return false;
		
		vector hit_pos = target.GetCursorHitPos();
		if (hit_pos == vector.Zero)
			return false;

		string surfType;
		g_Game.SurfaceGetType3D(hit_pos[0], hit_pos[1] + 0.1, hit_pos[2], surfType);
		
		hit_pos[1] = g_Game.SurfaceY(hit_pos[0],hit_pos[2]);

		return ( surfType.Contains("CRForest1") || surfType.Contains("CRForest2") || surfType.Contains("cp_broadleaf_sparse1") || surfType.Contains("cp_broadleaf_sparse2") || surfType.Contains("cp_broadleaf_dense1") || surfType.Contains("cp_broadleaf_dense2") || surfType.Contains("cp_conifer_common1") || surfType.Contains("cp_conifer_common2") || surfType.Contains("cp_conifer_moss1") || surfType.Contains("cp_conifer_moss2") );
	}

	override void OnFinishProgressServer( ActionData action_data )
	{	
		action_data.m_Player.GetStaminaHandler().SetStamina(0);
		float random = Math.RandomInt(0, 100);
		string theitem; 
		if (random <= 50) {
			theitem = "Bark_Oak";
		} 
		else 
		{
			theitem = "Bark_Birch";
		}
		ItemBase.Cast(HumanInventory.Cast(action_data.m_Player.GetInventory()).CreateInHands(theitem));
		action_data.m_Player.GetSoftSkillsManager().AddSpecialty( m_SpecialtyWeight );
	}
};









