class CfgPatches
{
	class dzr_find_bark
	{
		requiredAddons[] = {"DZ_Data", "DZ_Scripts", "DZ_Weapons_Melee", "DZ_Gear_Tools"};
		units[] = {};
		weapons[] = {};
	};
};
class CfgMods
{
	class dzr_find_bark
	{
		type = "mod";
		author = "DayZ Russia";
		dir = "dzr_find_bark";
		name = "dzr_find_bark";
		dependencies[] = {"World"};
		class defs
		{
			class worldScriptModule
			{
				files[] = {"dzr_find_bark/4_World"};
			};
		};
	};
};
