class CfgPatches
{
	class dzr_better_long_stick
	{
		requiredAddons[] = {"DZ_Data", "DZ_Scripts", "DZ_Weapons_Melee", "DZ_Gear_Tools", "DZ_Gear_Food"};
		units[] = {};
		weapons[] = {};
	};
};

class CfgMods
{
	class dzr_better_long_stick
	{
		type = "mod";
		author = "DayZ Russia";
		dir = "dzr_better_long_stick";
		name = "dzr_better_long_stick";
		dependencies[] = {"World"};
		class defs
		{
			class worldScriptModule
			{
				files[] = {"dzr_better_long_stick/4_World"};
			};
		};
	};
};

class CfgSlots
{
	class Slot_Ingredient2
	{
		name = "Ingredient2";
		displayName = "#STR_CfgFood0";
		ghostIcon = "set:dayz_inventory image:food";
	};
	class Slot_Ingredient3
	{
		name = "Ingredient3";
		displayName = "#STR_CfgFood0";
		ghostIcon = "set:dayz_inventory image:food";
	};
};

class CfgVehicles
{
	class Inventory_Base;
	class LongWoodenStick: Inventory_Base
	{
		model = "dzr_better_long_stick\dzr_woodenstick.p3d";
		attachments[] = {"Ingredient"};
	};
	class SharpWoodenStick: Inventory_Base
	{
		model = "dzr_better_long_stick\dzr_woodenstick.p3d";
		//attachments[] = {"DirectCookingA","DirectCookingB","DirectCookingC"};
		attachments[] = {"Ingredient"};
	};
};