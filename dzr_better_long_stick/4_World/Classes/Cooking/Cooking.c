//TODO
// Replace complex cooking with infinite action by 30 second intervals.
// setup 3 items.
// if each is true = cook.
// Get time to cook, temp to cook min
// Add temp all the time
// Check time and update stage
// Remove agents
// Don't save time.
modded class Cooking
{
	//Returns 1 if the item changed its cooking stage, 0 if not
	override int CookOnStick( Edible_Base item_to_cook, float cook_time_inc )
	{
		Print("CookOnStick: "+item_to_cook);
		if ( item_to_cook && item_to_cook.CanBeCookedOnStick() )
		{
			//update food
			return UpdateCookingStateOnStick( item_to_cook, cook_time_inc );
		}
		Print("item_to_cook && item_to_cook.CanBeCookedOnStick() == false: "+item_to_cook);
		return 0;
	}
	
	//Returns 1 if the item changed its cooking stage, 0 if not
	protected int UpdateCookingStateOnStick( Edible_Base item_to_cook, float cook_time_inc )
	{
		//food properties
		float food_temperature = item_to_cook.GetTemperature();
		
		//{min_temperature, time_to_cook, max_temperature (optional)}
		//get next stage name - if next stage is not defined, next stage name will be empty "" and no cooking properties (food_min_temp, food_time_to_cook, food_max_temp) will be set
		FoodStageType new_stage_type = item_to_cook.GetNextFoodStageType( CookingMethodType.BAKING );
		float food_min_temp = 0;
		float food_time_to_cook = 0;
		float food_max_temp = -1;
		bool is_done = false;	// baked
		bool is_burned = false;	// burned

		//Set next stage cooking properties if next stage possible
		if ( item_to_cook.CanChangeToNewStage( CookingMethodType.BAKING ) )
		{
			array<float> next_stage_cooking_properties = new array<float>;
			next_stage_cooking_properties = FoodStage.GetAllCookingPropertiesForStage( new_stage_type, null, item_to_cook.GetType() );
			
			food_min_temp = next_stage_cooking_properties.Get( eCookingPropertyIndices.MIN_TEMP );
			food_time_to_cook = next_stage_cooking_properties.Get( eCookingPropertyIndices.COOK_TIME );
			// The last element is optional and might not exist
			if ( next_stage_cooking_properties.Count() > 2 )
				food_max_temp = next_stage_cooking_properties.Get( eCookingPropertyIndices.MAX_TEMP );
		}
		
		
		// refresh audio
		if ( item_to_cook.GetInventory().IsAttachment() )
		{
			InventoryLocation invloc = new InventoryLocation;
			item_to_cook.GetInventory().GetCurrentInventoryLocation( invloc );
			if ( invloc )
			{
			/*
				if ( InventorySlots.GetSlotName( invloc.GetSlot() ) != "Ingredient" )
				{
					item_to_cook.MakeSoundsOnClient( true );
				}
				else
				{
			*/
					//add temperature
					item_to_cook.MakeSoundsOnClient( true );
					AddTemperatureToItem( item_to_cook, NULL, food_min_temp );
			/*			
				}
			*/
			};
		};
		
		//add cooking time if the food can be cooked by this method
		if ( food_min_temp > 0 && food_temperature >= food_min_temp )
		{
			float new_cooking_time = item_to_cook.GetCookingTime() + cook_time_inc;
			item_to_cook.SetCookingTime( new_cooking_time );
			
			//progress to next stage
			if ( item_to_cook.GetCookingTime() >= food_time_to_cook )
			{
				//if max temp is defined check next food stage
				if ( food_max_temp >= 0 )
				{
					if ( food_temperature > food_max_temp && item_to_cook.GetFoodStageType() != FoodStageType.BURNED )
					{
						new_stage_type = FoodStageType.BURNED;
					};
				};
				
				//change food stage
				item_to_cook.ChangeFoodStage( new_stage_type );
				//Temp
				//Remove all modifiers
				item_to_cook.RemoveAllAgentsExcept(eAgents.BRAIN);

				//reset cooking time
				item_to_cook.SetCookingTime( 0 );
				return 1;
			};
		};
		
		return 0;
	};

	
};