class CfgPatches
{
	class dzr_bury_anywhere
	{
		requiredAddons[] = {"DZ_Data", "DZ_Scripts", "DZ_Weapons_Melee", "DZ_Gear_Tools"};
		units[] = {};
		weapons[] = {};
	};
};
class CfgMods
{
	class dzr_bury_anywhere
	{
		type = "mod";
		author = "DayZ Russia";
		dir = "dzr_bury_anywhere";
		name = "dzr_bury_anywhere";
		dependencies[] = {"World"};
		class defs
		{
			class worldScriptModule
			{
				files[] = {"dzr_bury_anywhere/4_World"};
			};
		};
	};
};


