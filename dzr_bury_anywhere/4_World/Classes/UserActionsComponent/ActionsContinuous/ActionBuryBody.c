modded class ActionBuryBody
{
	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		if ( player.IsPlacingLocal() )
			return false;
		
		EntityAI body_EAI;
		Class.CastTo(body_EAI, target.GetObject());
		
		if ( body_EAI  &&  !body_EAI.IsAlive()  &&  (body_EAI.IsInherited(DayZCreature )  ||  body_EAI.IsInherited(Man)) && !body_EAI.GetParent() )
		{
			//string surface_type;
			//vector position = body_EAI.GetPosition();
			//GetGame().SurfaceGetType ( position[0], position[2], surface_type );
			
			//if ( GetGame().IsSurfaceDigable(surface_type) )
				return true;
		}
		
		return false;
	}
}