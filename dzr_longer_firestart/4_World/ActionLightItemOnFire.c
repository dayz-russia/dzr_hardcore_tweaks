modded class ActionLightItemOnFireCB : ActionContinuousBaseCB
{
	float thetime = UATimeSpent.FIREPLACE_IGNITE;
	string theitem = m_ActionData.m_Player.GetItemInHands().GetType();
	override void CreateActionComponent()
	{
		if( theitem == "HandDrillKit" )
		{
			thetime = 125;
		}
		if( theitem == "Matchbox" )
		{
			thetime = 17;
		}
		m_ActionData.m_ActionComponent = new CAContinuousTime(thetime);
	}
};

modded class ActionLightItemOnFire: ActionContinuousBase
{
	override void OnFinishProgressServer( ActionData action_data )
	{
		super.OnFinishProgressServer(action_data);
		
		if( action_data.m_MainItem.GetType() == "HandDrillKit" )
		{
			action_data.m_Player.GetStaminaHandler().SetStamina(0);
		}
	}		
};