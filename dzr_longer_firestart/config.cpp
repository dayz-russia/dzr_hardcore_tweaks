class CfgPatches
{
	class dzr_longer_firestart
	{
		requiredAddons[] = {"DZ_Data", "DZ_Scripts", "DZ_Weapons_Melee", "DZ_Gear_Tools"};
		units[] = {};
		weapons[] = {};
	};
};
class CfgMods
{
	class dzr_longer_firestart
	{
		type = "mod";
		author = "DayZ Russia";
		dir = "dzr_longer_firestart";
		name = "dzr_longer_firestart";
		dependencies[] = {"World"};
		class defs
		{
			class worldScriptModule
			{
				files[] = {"dzr_longer_firestart/4_World"};
			};
		};
	};
};
